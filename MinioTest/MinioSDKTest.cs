using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MinioTutorial.Utils;
using System;
using System.IO;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace MinioTest
{
    /// <summary>
    /// Minio Client API 예제 코드를 테스트하는 클래스입니다.
    /// </summary>
    /// <see cref="https://docs.min.io/docs/dotnet-client-api-reference.html"/>
    [TestClass]
    public class MinioSDKTest
    {
        private string currrentPath = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "..\\", "..\\", "..\\"));
        private StorageManager storage;

        /// <summary>
        /// Minio client 객체를 초기화합니다.
        /// </summary>
        [TestInitialize]
        public void TestInitialize()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            var section = config.GetSection(nameof(MinioConfig));

            var minioConfig = section.Get<MinioConfig>();
            storage = new StorageManager(minioConfig);
        }

        /// <summary>
        /// 저장소의 버킷 목록을 불러옵니다.
        /// </summary>
        [TestMethod]
        public async Task ListBuckets()
        {
            var result = await storage.minio.ListBucketsAsync();

            Assert.AreEqual(result.Buckets.Count > 0, true);
            Assert.AreEqual(result.Buckets[0].Name, "glovis");
        }

        /// <summary>
        /// glovis 버킷이 존재하는지 확인합니다.
        /// </summary>
        [TestMethod]
        public async Task BucketExists()
        {
            var result = await storage.minio.BucketExistsAsync("glovis");

            Assert.AreEqual(result, true);
        }

        /// <summary>
        /// 버킷을 정상적으로 생성하고 삭제할 수 있는지 확인합니다.
        /// </summary>
        [TestMethod]
        public async Task CreateAndRemoveBucket()
        {
            string bucketName = "test-bucket";

            bool isExist = await storage.minio.BucketExistsAsync(bucketName);
            Assert.AreEqual(isExist, false);

            await storage.minio.MakeBucketAsync(bucketName);
            isExist = await storage.minio.BucketExistsAsync(bucketName);
            Assert.AreEqual(isExist, true);

            await storage.minio.RemoveBucketAsync(bucketName);
            isExist = await storage.minio.BucketExistsAsync(bucketName);
            Assert.AreEqual(isExist, false);
        }

        /// <summary>
        /// glovis 버킷에서 객체 목록을 불러옵니다.
        /// </summary>
        [TestMethod]
        public async Task ListObjects()
        {
            var observable = storage.minio.ListObjectsAsync("glovis");
            var objects = await observable.ToList();

            Assert.AreEqual(objects.Count > 0, true);
        }

        /// <summary>
        /// glovis 버킷에서 객체를 다운로드 합니다.
        /// </summary>
        [TestMethod]
        public async Task GetObject()
        {
            // SEE: MinioTest\downloaded.jpg
            await storage.minio.GetObjectAsync("glovis", "unnamed.jpg", Path.Join(currrentPath, "downloaded.jpg"));
        }

        /// <summary>
        /// glovis 버킷에서 객체를 업로드 한 후 다운로드 합니다.
        /// </summary>
        [TestMethod]
        public async Task PutAndGetObject()
        {
            await storage.minio.PutObjectAsync("glovis", "lorem.txt", Path.Join(currrentPath, "lorem.txt"));
            await storage.minio.StatObjectAsync("glovis", "lorem.txt");

            await storage.minio.GetObjectAsync("glovis", "lorem.txt", 0L, 21L,
                stream => {
                    StreamReader reader = new StreamReader(stream);
                    string downloaded = reader.ReadToEnd();

                    Assert.AreEqual(downloaded, "Lorem ipsum dolor sit");
                });

            await storage.minio.RemoveObjectAsync("glovis", "lorem.txt");

            await Assert.ThrowsExceptionAsync<Minio.Exceptions.ObjectNotFoundException>(
                () => storage.minio.StatObjectAsync("glovis", "lorem.txt"));
        }
    }
}
