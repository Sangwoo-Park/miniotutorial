﻿using Minio;

namespace MinioTutorial.Utils
{
    public sealed class StorageManager
    {
        public MinioClient minio { get; set; }

        public StorageManager(MinioConfig minioConfig)
        {
            minio = new MinioClient(
                $"{minioConfig.host}:{minioConfig.port}",
                minioConfig.accessKey,
                minioConfig.secretKey);
        }
    }
}
