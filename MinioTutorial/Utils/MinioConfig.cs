﻿namespace MinioTutorial.Utils
{
    /// <summary>
    /// This object stores information for accessing the MINIO storage.
    /// </summary>
    public class MinioConfig
    {
        public string host { get; set; }
        public int port { get; set; }
        public string accessKey { get; set; }
        public string secretKey { get; set; }
        public string bucket { get; set; }
    }
}
