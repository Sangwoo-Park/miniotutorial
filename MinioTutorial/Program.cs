﻿using Microsoft.Extensions.Configuration;
using Minio.DataModel;
using Minio.Exceptions;
using MinioTutorial.Utils;
using System;

namespace MinioTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            var minioConfig = ReadMinioConfig();
            var storage = new StorageManager(minioConfig);

            // List buckets.
            Console.WriteLine($"- List Buckets");
            Console.WriteLine($"  Bucket Name\t\tCreation DateTime");
            var listBucketsTask = storage.minio.ListBucketsAsync();
            foreach(var bucket in listBucketsTask.Result.Buckets)
            {
                Console.WriteLine($"  {bucket.Name}\t\t{bucket.CreationDateDateTime}");
            }
            
        }

        /// <summary>
        /// Read configs from <c>appsetting.json</c> and <c>secrets.json</c>.
        /// If you want to change the secret, right click the project and select <c>Manage User Secrets</c>.
        /// </summary>
        /// <returns>A <c>MinioConfig</c> object.</returns>
        static MinioConfig ReadMinioConfig()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .AddUserSecrets<Program>()
                .Build();

            var section = config.GetSection(nameof(MinioConfig));

            return section.Get<MinioConfig>();
        }
    }
}
