# MinIO .NET Tutorial
이 저장소는 MinIO의 .NET snippet을 유닛 테스트 형식으로 제공합니다.

완전한 API 레퍼런스는 공식 사이트의 [.NET Client API Reference](https://docs.min.io/docs/dotnet-client-api-reference.html)를 참조하십시오.

이 프로젝트는 MinIO client를 멤버 변수로 갖는 간단한 클래스 `StorageManager`와 환경 설정 클래스인 `MinioConfig`를 정의합니다.

`StorageManager` 클래스는 **시연만을 위해 제작된 클래스**로, **개발 환경에 그대로 적용하기에는 부적절한 구조**임을 미리 말씀드립니다.

## MinioTest Project
이 프로젝트에서는 MinIO의 API를 시연하기 위하여 Unit Test Project를 사용하였습니다. `Solution Explorer`의 하단 메뉴 중, `Test Explorer`를 눌러 코드를 실행해 볼 수 있습니다.

MinioSDKTest 클래스에는 총 6 가지의 테스트 코드를 사용해 데모 코드를 실행시킵니다.

### Bucket Operations
#### 1. ListBuckets
이 테스트는 `ListBucketsAsync()` 메소드를 사용하여 버킷 목록을 불러온 후, 목록의 갯수가 0보다 큰지, `glovis` 버킷을 포함하는지 테스트합니다.

#### 2. BucketExists
이 테스트는 `BucketExistsAsync()` 메소드를 사용하여 glovis 버킷이 존재하는지 검사합니다.

#### 3. CreateAndRemoveBucket
이 테스트는 `BucketExistsAsync()` 메소드를 사용하여 이름이 `test-bucket`인 버킷이 존재하는지 검사합니다. 이후 `MakeBucketAsync()` 메소드를 사용하여 버킷을 생성한 후, 다시 `BucketExistsAsync()` 메소드를 사용하여 버킷이 올바르게 생성되었는지 검사합니다. 마지막으로, `RemoveBucketAsync()` 메소드를 사용하여 버킷을 제거한 후 올바르게 제거되었는지 확인합니다.

### Object Operations
#### 4. ListObjects
이 테스트는 `ListObjectsAsync()` 메소드를 사용하여 객체 목록을 받아와, `glovis` 버킷에 객체가 한 개 이상 존재하는지 검사합니다.
이 메소드는 ReactiveX .net의 `IObservable` 객체를 반환합니다. ReactiveX 라이브러리의 사용법에 익숙하지 않다면, 반드시 [Rx.net 저장소](https://github.com/dotnet/reactive)를 참조하십시오.

#### 5. GetObject
이 테스트는 `GetObjectAsync()` 메소드를 사용하여 `glovis` 버킷의 `unnamed.jpg` 객체를 `downloaded.jpg` 라는 이름으로 내려받아 저장합니다.

#### 6. PutAndGetObject
이 테스트는 `glovis` 버킷에 `lorem.txt` 파일을 동일한 키를 갖는 객체로 업로드한 후, `StatObjectAsync()` 메소드를 사용해 올바르게 업로드 되었는지 확인합니다.
이후 `GetObjectAsync()` 메소드를 사용해 업로드 된 `lorem.txt` 객체를 0 바이트 부터 21 바이트 까지 부분적으로 읽어와, 해당 문자열이 Lorem ipsum dolor sit인지 확인합니다.
마지막으로 해당 객체를 삭제한 후, 올바르게 삭제되었는지 확인합니다.
